class Queue {
  constructor(...items) {
    // max length
    this._maxlen = null
    //initialize the items in queue
    this._items = []
    // enqueuing the items passed to the constructor
    this.enqueue(...items)
  }

  setMaxlen(size) {
    this._maxlen = size;
  }

  enqueue(...items) {
    //Max of items, then remove first item of queue
    if (this._maxlen && this._items.length == this._maxlen) this.dequeue();
    //push items into the queue
    items.forEach(item => this._items.push(item))
    return this._items;
  }

  dequeue(count = 1) {
    //pull out the first item from the queue
    this._items.splice(0, count);
    return this._items;
  }

  peek() {
    //peek at the first item from the queue
    return this._items[0]
  }

  size() {
    //get the length of queue
    return this._items.length
  }

  show() {
    console.log(this._items)
    //this._items.forEach( item => console.log(item) )
  }

  isEmpty() {
    //find whether the queue is empty or no
    return this._items.length === 0
  }
}
class statsQueue extends Queue {
  constructor(...items) {
    super(...items)
  }

  getAvarageTime() {
    let total_duration = 0
    let notduration = 0;
    this._items.forEach((item) => {
      if (!item.duration) notduration++;
      else if (item.duration == 0) notduration++;
      else total_duration += item.duration
    })
    if (this._items.length == 0) return 0;
    return total_duration / (this._items.length - notduration);
  }

}
/**
 * Example
    let my_queue = new Queue(1,24,4);
    // [1, 24, 4]
    my_queue.enqueue(23)
    //[1, 24, 4, 23]
    my_queue.enqueue(1,2,342);
    //[1, 24, 4, 23, 1, 2, 342]
    my_queue.dequeue();
    //[24, 4, 23, 1, 2, 342]
    my_queue.dequeue(3)
    //[1, 2, 342]
    my_queue.isEmpty()
    // false
    my_queue.size();
//3
 */

module.exports = statsQueue;
