package watchman

import k "kumori.systems/kumori/kmv"

#Manifest:  k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.minifaas"
    name: "faas_watchman"
    version: [0,0,1]
  }

  description: {

    srv: {
      
      client: natsclient: {
        protocol: "tcp"
      }

    }

    config: {
      resource: {}
      parameter: {
       config: {}
        urlnats: "0.natsclient"
      }
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: watchman: k.#Container & {
      name: "watchman"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "mamarbao/faas-watchman:9.1"
      }
      mapping: {
        filesystem: [
          {
            path: "/config/config.json"
            data: config.parameter.config
            format: "json"
          },
        ]
        env: {
          URL_NAT: "0.natsclient",
          PORT_NAT: "80"
        }
      }
    }
  }
}
