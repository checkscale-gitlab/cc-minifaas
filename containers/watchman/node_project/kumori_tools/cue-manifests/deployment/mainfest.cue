package deployment

import (
  k "kumori.systems/kumori/kmv"
  c "kumori.systems/minifaas/service:faas_service"
)

#Manifest: k.#MakeDeployment & {

  _params: {
    ref: {
      domain: "kumori.systems.minifaas"
      name: "faas_deploy"
      version: [0,0,1]
    }
    
    inservice: c.#Manifest & {
      description: role: db: rsize: $_instances: 3
      description: role: nats: rsize: $_instances: 3
      description: role: frontend: rsize: $_instances: 2
	    description: role: worker: rsize: $_instances: 2
      description: role: watchman: rsize: $_instances: 1
    }
    config: {
      parameter: {
        frontend: {
          restapiclientPortEnv: "80"
        }
      }
    }
  }
}
