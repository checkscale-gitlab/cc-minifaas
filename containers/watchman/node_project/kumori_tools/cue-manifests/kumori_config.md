
// [PWD] manu@pop-os:~/Documents/MASTER/CC/Kumori/CC-miniFaas$ 

//0
kumorictl config --user-domain minifaas-cm2
//1 opcional, hacer despues
kumorictl register certificate faascert.wildcard \
--domain *.vera.kumori.cloud \
--cert-file ./certs/faascert.wildcard \
--key-file ./certs/faascert.wildcard.key
//2

kumorictl register http-inbound minifaas-cm2/faas-inbound --domain minifaas-cm2.vera.kumori.cloud --cert minifaas-cm2/faascert.wildcard --comment "Inbound FaaS"

//3

kumorictl register deployment minifaas-cm2/faas-deploy --deployment ./cue-manifests/deployment --comment "Deployment FaaS"

//4

kumorictl link minifaas-cm2/faas-deploy:service minifaas-cm2/faas-inbound

//Comprobar lanzamineto

kumorictl describe deployment minifaas-cm2/faas-deploy


kumorictl exec -t minifaas-cm2/faas-deploy frontend instance-66b49765f8-4ctvc frontend sh

kumorictl logs minifaas-cm2/faas-deploy frontend instance-f68844fb6-fxvpp

//solo deployment eliminar 

kumorictl unregister deployment minifaas-cm2/faas-deploy

//4
kumorictl register certificate faascert.wildcard --domain *.vera.kumori.cloud --cert-file ./certs/faascert.wildcard --key-file ./certs/faascert.wildcard.key
//5
kumorictl register http-inbound minifaas-cm2/faas-inbound --domain minifaas-cm2.vera.kumori.cloud --cert minifaas-cm2/faascert.wildcard --comment "Inbound FaaS"
//8 Eliminar con link
kumorictl unlink minifaas-cm2/faas-deploy:service minifaas-cm2/faas-inbound
kumorictl unregister deployment minifaas-cm2/faas-deploy  
