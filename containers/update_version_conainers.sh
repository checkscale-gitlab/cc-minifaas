#!/bin/bash
version=$1
userDockerHub=mamarbao/
# Docker file based on base
nameFront=faas-frontend
nameWorker=faas-worker
nameDB=faas-db
nameNats=faas-nats
nameWatchman=faas-watchman

imgFront="$userDockerHub$nameFront":$version
imgWorker="$userDockerHub$nameWorker":$version
imgDB="$userDockerHub$nameDB":$version
imgNats="$userDockerHub$nameNats":$version
imgWatchman="$userDockerHub$nameWatchman":$version

checkParam(){
    if [ -z $version ]; then echo "No version has been specified"; exit 127; fi
}
checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while building image dockers: $2"
		exit 127
	fi
}
list-images(){
for i in $imgFront $imgWorker $imgDB $imgNats $imgWatchman; do
 echo $i
done
}
build-image(){
declare -A my_array
my_array=([$imgFront]="./frontend/" [$imgWorker]="./worker/"  [$imgWatchman]="./watchman/" )
for img in "${!my_array[@]}"; do
    echo "[0 CREATE] Image $img if it not exist.. ${my_array[$img]}"
    if [[ "$(docker images -q $img 2> /dev/null)" == "" ]]; then
        ts=$(date +%s); 
        docker build --no-cache -t $img  ${my_array[$img]} &> /dev/null
        checkerror $?
        tt=$((($(date +%s) - $ts))) ; 
        echo "[1 CREATED] Image: $img - Time taken: $tt s"
        echo "[2 PUSH] Image: $img"
        docker push $img 
        checkerror $?
        echo "[3 UPLOAD] Image: $img to dockerhub $userDockerHub"  
    else
        echo "[1 NOT CREATED] Image: $img - already exists"
    fi
done
}
#
# === MAIN ===
#
checkParam
#if [ $(id -u) -ne 0 ]; then
#	echo "[WARN] Run this script as a Root user only" >&2
#	exit 1
#fi

#----------------------------------
# List
# --------------------------------
list-images

#----------------------------------
# Image Build image
# --------------------------------
build-image

