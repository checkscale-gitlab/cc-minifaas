import express from 'express';
const router = express.Router();
import {Client, connect, NatsConnectionOptions, Payload} from 'ts-nats';
const server_nat = String(process.env.SERVER_NAT || '0.0.0.0');
const MILLIS_BETWEEN_REQUESTS= Number(process.env.MILLIS_BETWEEN_REQUESTS || '5000')
var nats: Client;
router.get('/startlaunch/ros', async (req, res) => {
    //if (req.body ==null) res.send({ message: "Error Not found JavaScript Code" , succes: false})
    let { user_name } = req.query;
    
    if(!nats){
        nats = await connect({servers: ['nats://'+server_nat+':4222']});
    }
    /**
     *  Enviar codigo a la cola NATS
     */
   let msg = await nats.request('fibonacci',MILLIS_BETWEEN_REQUESTS, '4');   
   console.log(msg)
    res.send(msg);
});

module.exports = router;