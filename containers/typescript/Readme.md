Init TypeScriptProject

npm i -D typescript ts-node-dev @types/express

It transpiles down to plain JavaScript and allows us to build scalable and enterprise grade applications with JavaScript via the TypeScript compiler . Then these libraries are not required after the code is transpiled from TypeScript to JavaScript meaning they are just bloating our image.

Multi-stage strategy will be:

1- The builder stage:

Install all dependencies including TypeScript
Transpile the code into JavaScript and save it in a directory x

2 -The final stage will:

Copy the directory where TypeScript compiler has saved the new transpile code from the builder stage
Install ONLY the dependencies required in production
Use the copied JavaScript code to run the application


Let’s build the image using:

docker build -t <docker-username>/typescriptapi:latest .

Note the tag name 'latest' is not necessary, the docker deamon assognt the tag 'latest' by default.

Since we are using a multi-stage strategy, we could target and build a specific stage. This allows to build development versions of our images for inspection purposes. For example, It's possible to target the builder stage using:

docker build --target builder -t <docker-username>/typescriptapi:dev 

Run: 
docker run -p 3100:3100 -e NODE_ENV=production test/typescriptapi 

inspect image:docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
test/typescriptapi               latest              b2479d240fd3        2 minutes ago       154MB
node                             14-alpine           fa2fa5d4e6f4        3 days ago          117MB

image weighs in at an impressive 154 MB, 117MB is a base image and 37MB is source code, node packages, OS updates and configuration.

Remove the stopped container and all of the images, including unused or dangling images, with the following command:

docker system prune -a


Future improvements

It's possible to incremente the performance express api thanks to use [Fastify](https://www.fastify.io/benchmarks/)