#!/bin/sh
set -eu
if [ "${1-}" = "shell" ]; then
  shift
  exec /bin/sh "$@"
else
  echo "[INIT] RUN INIT SCRIPT"
  sleep $WAIT
  myip=`hostname -I | tr -d '[[:space:]]'`
  nodo=${HOSTNAME:${#HOSTNAME}-1:1}
  #GET IP MAIN SERVER DB VIA ENVIRONMENT VARIABLE -> INSTANCE (NUM of current instances)
  ip_maindb=$(nslookup $CHANNEL | awk '/Address/{print}' | sed 's/Address: //' | tail -n $INSTANCE | awk '{print}' ORS=',' | sed 's/.$//')
  #Information
  echo "[0] CH: $CHANNEL MAIN INS: $INSTANCE IPS: $ip_maindb  IP_HOST: $myip Nodo: $nodo"
  # --join all to maindb
  ./cockroach start --insecure --listen-addr=$myip:26257 --http-addr=$myip:8080 --join=$ip_maindb --background
  #INIT SERVICE ONLY MAIN DB = NODO 0
  if [ "$nodo" = "0" ]; then
      echo "[1] Main db, trying to init db"
      sleep 20
      ./cockroach init --insecure --host=$myip:26257
  else
      echo "[1] Node not main, trying to connect to main db"
  fi

#Check each 60s status node cockroachdb
  while sleep 60; do
  if pgrep "cockroach" >/dev/null 2>&1
    then
        echo "[2] cockroach service OKEY:"
        ./cockroach node status --host=$myip:26257 --insecure
    else
      echo "[2] cockroach service not working FAILED"
      exit 1
  fi
  done
fi
