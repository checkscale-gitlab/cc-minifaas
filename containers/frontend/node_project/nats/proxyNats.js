const NATS = require('nats');
//KUmori: URL_NAT: 0.natsclient (canal client)
const URL_NAT = process.env.URL_NAT || '0.0.0.0';
const PORT_NAT = process.env.PORT_NAT || '4222';
var NATS_URL = `nats://${URL_NAT}:${PORT_NAT}`;

class ProxyNats {

  constructor(timeout = 500) {
    this.ConnectionClient = null
    this.timeout = timeout
    this.url = NATS_URL;
    this.nats = null;
  }

  connectNATS(maxRetries = 3, retryInterval = 5000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        _self.nats = NATS.connect({ url: NATS_URL, maxReconnectAttempts: 10, reconnectTimeWait: 350, verbose: true });
        _self.nats.on('error', (err) => {
          console.log(`[ERROR CONNECT NATS][RETRY:${maxRetries}] ${err}`)
          if (maxRetries == 0) {
            if (!_self.nats.connected)
              reject('Error when trying to connecte to nats');
          } else resolve(_self.connectNATS(maxRetries - 1, retryInterval));
        })

        _self.nats.on('close', function () {
          console.log('[INFO NATS] close')
        })
        // emitted whenever the client is attempting to reconnect
        _self.nats.on('reconnecting', () => {
          console.log('[INFO NATS] reconnecting')
        })

        _self.nats.on("connect", () => {
          console.log('[SUCCESS NATS] NATS Frontend connected')
          resolve(_self.nats)
        });
      }, retryInterval);
    });
  }
  

  requestStatsFrame(message, timout = 8000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      //just to receive a response message { max: 1 } and after close request
      let ssid = _self.nats.request('stats-frame', JSON.stringify({username:"mamarbao"}), { max: 1 }, function (response) {
        resolve(response)
      })
      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        reject('Function (requestStatsFrame) timed out')
      });
    });
  }

  requestDecision(message, timout = 165000) {
    var _self = this;
    console.log(`[F] requestDecision until ${timout}`)
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      //just to receive a response message { max: 1 } and after close request
      let ssid = _self.nats.request('stats-take-decision', JSON.stringify(message), { max: 1 }, function (response) {
        resolve(response)
      })
      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        reject('Function (requestDecision) timed out')
      });
    });
  }

  requestSolDecision(message, timout = 165000) {
    var _self = this;
    console.log(`[F] requestSolDecision until ${timout}`)
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      //just to receive a response message { max: 1 } and after close request
      let ssid = _self.nats.request('stats-take-decision-result', JSON.stringify(message), { max: 1 }, function (response) {
        resolve(response)
      })
      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        reject('Function (requestDecision) timed out')
      });
    });
  }


  requestFunc(message, timout = 20000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      console.log('[F] Request to (launch) queue')
      //just to receive a response message
      let ssid = _self.nats.request('launch', JSON.stringify(message), { max: 1 }, function (response) {
        resolve(response)
      })

      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        console.log('ssid ' + ssid + ' timed out');
        reject('Function timed out')
      });
    });
  }

  requestMaths(message, timout = 20000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      console.log(' params, ',message.params)
      //just to receive a response message { max: 1 } and after close request
      let ssid = _self.nats.request('launch-maths', JSON.stringify(message), { max: 1 }, function (response) {
        resolve(response)
      })
      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        reject('Function timed out')
      });
    });
  }


  pubStats(message) {
    var _self = this;
    if (!_self.nats) throw new Error('Necessary firts connection with Nats')
    if (!_self.nats.connected) throw new Error('Not connected to NATS Server')
    _self.nats.publish('stats-clients', JSON.stringify(message), function (response) {
      console.log('Published [stats-clients] : "' + message + '"')
    })
  }
}


class NatsSingleton {
  constructor() {
    throw new Error('Use Singleton.getInstance()');
  }
  static getInstance(timeout = 500) {
    if (!NatsSingleton.instance) {
      NatsSingleton.instance = new ProxyNats(timeout);
    }
    return NatsSingleton.instance;
  }
}

module.exports.instance = (timeout = 500) => {
  return NatsSingleton.getInstance(timeout = 500);
}