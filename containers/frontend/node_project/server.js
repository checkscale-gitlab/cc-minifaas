var express = require("express"),
  path = require('path'),
  bodyParser = require("body-parser");
const ProxyWatchMan = require("./tools/watchman")
const CockroachDB = require("./db/cockroachDB");
const ProxyNats = require("./nats/proxyNats");
const { concatSeries } = require("async");

async function main() {
  const app = express();
  /**
   * PUNTOS PENDIENTES
   * 
   * 1. WatchMan
   */

  //1 Instace ProxyNats class (Singleton) only one
  var mProxyNats = await ProxyNats.instance();
  try {
    //2 Instace CockroachDB class (Singleton) only one
    // a - Firts init new CockroachDB() -> Init DB via sql
    // b - If Init okey return CLientDB with connection to db
    var mCockroachDB = await CockroachDB.instance();
  } catch (error) {
    console.log(`[Server F] DB: ${error}`)
    //It's possible to use retries
  }
  try {
    //3 ProxyNats instance init connection to NatServer
    await mProxyNats.connectNATS()
    //4 Instace ProxyWatchMan class (Singleton) only one
    // It's necessary ProxyNats connected to NatServer
    var mProxyWatchMan = await ProxyWatchMan.create(mProxyNats)
  } catch (error) {
    console.log(`[Server F] Nats: ${error}`)
    if (!process.env.NODE_ENV_TEST) process.exit(1)
  }
  // Setting Base directory
  app.use(bodyParser.json());
  //CORS Middleware
  //app.use(morgan('dev'));
  app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,DELETE,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
  });
  app.use(express.static('public'));
  app.use(bodyParser.urlencoded({ extended: true }));
  //Limit HTTP request size 5MB
  app.use(bodyParser.urlencoded({ limit: '15mb', extended: true }));
  app.use(bodyParser.json({ limit: '15mb' }));
  //Set STATS from WatchMAN
  //app.use(WatchMan.instance().logResponseTime)
  // Global variables

  app.use((req, res, next) => {
    const start = process.hrtime();
    //Up event when a response to client finish
    res.on('finish', () => {
      if (mProxyWatchMan)
        try {
          mProxyWatchMan.statsResponse(req, res, start)
        } catch (err) {
          console.log({ err })
        }
    })
    next();
  });
  app.get('/', function (req, res) {
    res.send(`<h>Hello from Frontend Faas</h1><p><h3>Node Instance : ${process.env.HOSTNAME}</h3>`);
  })


  let route_launch = require('./routes/action-launch')
  //                ServerExpress, CrockroachDB, Nats
  route_launch.load(app, mCockroachDB, mProxyNats)
  let route_db = require('./routes/action-db')
  route_db.load(app, mCockroachDB)

  //Error handler
  // To up handler using => next(String)
  app.use((error, req, res, next) => {
    // by default get the error message
    let err = error;
    console.log('[ERROR Handler]', error)
    let key = 'error';
    // for display purposes, if it's an array call it "errors"
    if (Array.isArray(error)) {
      key = 'errors';
    }

    return res.status(err.status || 500).json({ [key]: err, success: false });
  });

  var HOSTNAME=process.env.HOSTNAME
  var server = app.listen(process.env.PORT || 8080, process.env.HOST || '0.0.0.0', (err) => {
    if (err) {
      console.error('Error starting  server', err);
      return;
    }
    let port = server.address().port;
    let address = server.address().address;
    console.log("[HOST URL]", `http://${address}:${port}/`);
  });
}

main();