var async = require("async");
var dateFormat = require('dateformat');
var myConnection = require('./connection'); //path to above db module

class CockroachDB {

    constructor(resolve, reject) {
        this.ConnectionClient = null
        var self = this
        this.startConnection(self, resolve, reject)
    }

    async startConnection(_self, resolve, reject) {
        try {
            _self.ConnectionClient = await myConnection.init({});
            resolve(_self)
        } catch (err) {
            console.error("[ERROR CockroachDB]", err)
            resolve(_self)
        }
    }

    insert(queryInsert) {
        return new Promise(async (resolve, reject) => {
            async.waterfall([
                function (next) {
                    console.log(queryInsert)
                    myConnection.query(
                        queryInsert, next
                    );
                }],
                function (err, results) {
                    if (err) {
                        reject(err)
                    } else{
                        if(results.rowCount==0)
                        resolve({ message: `Without changes`, success: false });
                        else
                        resolve({ message: `Inserted correctly`, success: true });
                    }
                }
            );

        })
    }

    get(query) {
        return new Promise(async (resolve, reject) => {
            async.waterfall([
                function (next) {
                    console.log(query)
                    myConnection.query(
                        query, next
                    );
                }],
                function (err, results) {
                    if (err) {
                        reject(err)
                    } else
                        resolve({ message: results.rows, success: true });
                }
            );
        })
    }

    insertCheckUser(queryInsert) {
        return new Promise(async (resolve, reject) => {
            async.waterfall([
                function (next) {
                    console.log(queryInsert)
                    myConnection.query(
                        queryInsert, next
                    );
                }],
                function (err, results) {
                    if (err) {
                        reject(err)
                    } else{
                        if(results.rowCount==0)
                        resolve({ message: `User already exist`, success: false });
                        else
                        resolve({ message: `Registred correctly`, success: true, creator: results.rows.length==0?"":results.rows[0].user_id });
                    }
                }
            );

        })
    }

    insertFunc(func) {
        if(func.params && func.type=="maths")
            return this.insert(`INSERT INTO functions( type,function_name,code,creator,size,params) VALUES ('${func.type}','${func.function_name}', '${func.code}','${func.creator}',${func.size},'${func.params}')`)
        else
            return this.insert(`INSERT INTO functions( type,function_name,code,creator,size) VALUES ('${func.type}','${func.function_name}', '${func.code}','${func.creator}',${func.size})`)
        }

    insertUser(username) {
        return this.insertCheckUser(`INSERT INTO users(username) VALUES ('${username}') on conflict (username) do nothing RETURNING user_id;`)
    }

    //Insert resultados

    insertResult(function_id, result) {
        let date = dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        return this.insert(`INSERT INTO results (function_id, result, date) VALUES ('${function_id}', '${result}','${date}');`)
    }

    //Insert registro

    insertResource(user_id, function_id, time, instance) {
        let date = dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        return this.insert(`INSERT INTO registers(user_id, function_id, use_time, instance_type, date) VALUES ('${user_id}', '${function_id}', '${time}' ,'${instance}','${date}');`)
    }

    //Get UserID

    getUserID(username) {
        return this.get(`SELECT user_id FROM users WHERE username = '${username}'`)
    }

    getUserbyID(creator) {
        return this.get(`SELECT user_id FROM users WHERE user_id = '${creator}'`)
    }


    getUser(username) {
        return this.get(`SELECT user_id,username,user_rol FROM users WHERE username = '${username}'`)
    }

    getFunctionID(function_name) {
        return this.get(`SELECT function_id FROM functions WHERE function_name = '${function_name}'`)
    }

    getFuncByNameUser(function_name, username) {
        return this.get(`SELECT * FROM functions AS f 
                                  JOIN users AS u ON f.creator = u.user_id
                                  WHERE f.func_name = '${function_name}' AND u.username='${username}' ;`)
    }

    getFuncByName(name_function) {
        return this.get(`SELECT * FROM functions where function_name='${name_function}' ;`)
    }
    getSolFuncByNameUser(username) {
        return this.get(`SELECT r.result,r.date,f.function_name,u.username FROM results AS r
                                  JOIN functions AS f ON r.function_id = f.function_id
                                  JOIN users AS u ON f.creator = u.user_id
                                  WHERE u.username = '${username}' ;`)
    }
    getAllUsers() {
        return this.get(`SELECT * FROM users;`)
    }

    getAllFunc() {
        return this.get(`SELECT * FROM functions;`)
    }

    getAllSolFunc() {
        return this.get(`SELECT * FROM results;`)
    }

    getAllResources() {
        return this.get(`SELECT * FROM registers;`)
    }

    getResourcesFromUser(username) {
        return this.get(`SELECT f.function_name, f.size, r.use_time, r.instance_type FROM registers AS r 
                                    JOIN functions AS f ON r.function_id = f.function_id
                                    JOIN users AS u ON r.user_id = u.user_id
                                    WHERE u.username = '${username}';`)
    }

    getResultFromFunction(username, function_name) {
        return this.get(`SELECT r.result FROM results AS r
                                  JOIN functions AS f ON r.function_id = f.function_id
                                        WHERE f.function_name = '${function_name}' AND f.creator = (SELECT user_id FROM users WHERE username='${username}');`)
    }

}


class Singleton {
    constructor() {
        throw new Error('Use Singleton.getInstance()');
    }
    static getInstance() {
        return new Promise((resolve, reject) => {
            if (!Singleton.instance) {
                new CockroachDB((cockroachDB) => {
                    Singleton.instance = cockroachDB;
                    resolve(Singleton.instance)
                },
                    (err) => {
                        Singleton.instance = null;
                        reject(err)
                    });
            } else
                resolve(Singleton.instance)
        })
    }
}

module.exports.instance = () => {
    return Singleton.getInstance(); //return a promise
}
