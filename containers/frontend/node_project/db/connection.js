var pg = require("pg");
var async = require("async");
const fs = require('fs');
const path = require('path');
var pool;
var config = {
    user: process.env.DB_USER || "root",
    host: process.env.DB_HOST || "0.0.0.0",
    database: process.env.DB_DATABSE || "defaultdb",
    port: process.env.DB_PORT || 26257
};
var firstInit = true;

pool = new pg.Pool(config);
pool.on('error', function (err, client) { console.log("[DBConnection] POOL ERROR"); })

function initPromise() {
    return new Promise((resolve, reject) => {
        pool.connect(function (err, client, done) {
            // Close the connection to the database and exit
            var finish = function () {
                done();
                //process.exit();
            };

            if (err) {
                finish();
                return reject(err);
            } else {
                firstInit = false;
                console.log("[DBConnection] CONNECTED DB");
                console.log("[DBConnection] FIRST CONNECTION GENERATING DATA IF NOT EXITS");
                // async.waterfall is used to run a multiple task that is dependent to the previous one.
                async.waterfall(
                    [function (next) {
                        fs.readFile(path.resolve(__dirname, 'creates.sql'), 'UTF-8', next)
                        // the contents of file1.js

                    },
                    function (result, next) {
                        //result is the content of creates.sql
                        client.query(result, next);
                    },
                    function (results, next) {
                        // Insert three rows into the 'demo' table.
                        client.query(
                            "INSERT INTO users(username,user_rol) VALUES ('manager','admin') on conflict (username) do nothing;",
                            next
                        );
                    },
                    function (results, next) {
                        // Insert three rows into the 'demo' table.
                        client.query(
                            "INSERT INTO users(username) VALUES ('mamarbao') on conflict (username) do nothing RETURNING user_id;",
                            next
                        );
                    },
                    function (results, next) {
                        if (!results.rows[0]) next(null, "pass")
                        else {
                            let creator = results.rows[0].user_id;
                            // Insert three rows into the 'demo' table.
                            client.query(
                                `INSERT INTO functions(function_id,type,function_name,code,creator,size,params) VALUES (200,'javascript','suma', 'console.log(3+4)',${creator},500,NULL), (100,'maths','resta', '100-10',${creator},100,NULL), (101,'maths','areaTriangulo', '(a*b)/2',${creator},100,'a,b') on conflict (function_id) do nothing;`,
                                next
                            );
                        }
                    },
                    function (results, next) {
                        // Print the record inserted into the table
                        client.query("SELECT * FROM functions;", next);
                    }
                    ],
                    function (err, results) {
                        if (err) {
                            console.error("Error Inserting and Printing ", err);
                            firstInit = true;
                            return reject(err)
                        }
                        console.log("Sample Node with CockroachDB:");
                        results.rows.forEach(function (row) {
                            console.log(row);
                        });
                        firstInit = false
                        return resolve(pool);
                    }
                );

                client.on('error', function (error) {
                    //Error then close connection PoolClient
                    done();
                    return;
                });
                client.on('end', (message) => { console.error('[DBConnection] Client DB end event  (desconnected)'); client._connected = false })
            }
        });
    })
}

module.exports = {
    init: () => {
        return initPromise();
    },
    query: async (text, params, callback) => {
        const start = Date.now()
        //IN case of failure to initialise the database firstInit=true, then firstInit=false
        if (firstInit) {
            try {
                await initPromise();
            } catch (error) {
                console.log("[CONN] Trying to init db FAILED")
            }
        }
        return pool.query(text, params, (err, res) => { //Pool query similiar that client.query, but we use the default Poolclient
            const duration = Date.now() - start
            console.log('executed query', { text, duration, rows: res.rowCount })
            callback(err, res)
        })
    },
    //if we need to check out a client from the pool to run several queries in a row in a transaction. Here we can configure our PoolClient of Pool
    getClient: (callback) => {

        pool.connect((err, client, done) => {
            const query = client.query
            // monkey patch the query method to keep track of the last query executed
            client.query = (...args) => {
                client.lastQuery = args
                return query.apply(client, args)
            }
            // set a timeout of 5 seconds, after which we will log this client's last query
            const timeout = setTimeout(() => {
                console.error('A client has been checked out for more than 5 seconds!')
                console.error(`The last executed query on this client was: ${client.lastQuery}`)
            }, 5000)
            const release = (err) => {
                // call the actual 'done' method, returning this client to the pool
                done(err)
                clearTimeout(timeout)
                // set the query method back to its old un-monkey-patched version
                client.query = query
            }
            callback(err, client, release)
        })
    }

};