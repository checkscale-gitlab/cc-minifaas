const NATS = require('nats');
const nodeEval = require('node-eval');
const math = require('mathjs');
const exec = require('./commands');
const URL_NAT = process.env.URL_NAT || '0.0.0.0';
const PORT_NAT = process.env.PORT_NAT || '4222';
NATS_URL = `nats://${URL_NAT}:${PORT_NAT}`;
var nats;

const INSTANCE= process.env.INSTANCE || "unknown";


function connectNATS(maxRetries = 3, retryInterval = 5000) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      nats = NATS.connect({ url: NATS_URL, maxPingOut: 10, maxReconnectAttempts: -1, reconnectTimeWait: 250 });
      nats.on('error', (err) => {
        console.log(`[ERROR][RETRY:${maxRetries}] ${err}`)
        if (maxRetries == 0) {
          if (!nats.connected)
            reject('Error when trying to connecte to nats');
        } else resolve(connectNATS(maxRetries - 1, retryInterval));

      })
      // emitted whenever the client is attempting to reconnect
      nats.on('reconnecting', () => {
        console.log('[INFO] reconnecting')
      })

      nats.on("connect", () => {
        console.log('[SUCCESS] NATS Worker connected')
        resolve(nats)
      });

      nats.on('close', () => {
        console.log('[CLOSE] NATS Worker Closed')
        process.exit()
      })
    }, retryInterval);
  });
}

function disconnect() {
  if (nats) {
    nats.flush(() => {
      nats.close();
      nats = null;
    });
  }
}

function init_subs() {

  if (!nats.connected) return false;


  /**
   * The server will load balance between all members of the queue group. 
   * In a cluster setup, every member has the same chance of receiving a particular message
   * [https://docs.nats.io/developing-with-nats/receiving/queues]
   */
  nats.subscribe('launch-maths', { queue: 'job.mathematics.workers' }, (msg, replyTo) => {
    console.log(`requested the msg: (${msg})`)
    // MATHJS: https://mathjs.org/docs/expressions/parsing.html
    let valid_msg = check_message(msg);
    let scope;
    console.log("[msg valid]: ", valid_msg)
     // Start to do Work
    const start = process.hrtime();
    // Valid msg do Work
    if (valid_msg) {
      let result = {};
      try {
        let {value_params,params,code,function_id} = valid_msg;
        if(value_params && params){
          let my_array_values =convert_to_array_integers(value_params);
          scope=set_mathscope(my_array_values,params.split(","))
        }
        //EXEC VIA MATHS
        const node =  math.parse(code)
        const code_exec = node.compile()       // compile an expression
        result.message = scope==null?code_exec.evaluate():code_exec.evaluate(scope) // evaluate the code with an optional scope
        //FORMAT RESULT
        const durationInMilliseconds = getDurationInMilliseconds(start)
        result.duration = durationInMilliseconds
        result.function_id = function_id;
        result.success = true;
        result.instance=INSTANCE;
      } catch (e) {
        console.log(e)
        result = { success: false, message: "Error", function_id: valid_msg.function_id,instance:INSTANCE, error: JSON.stringify(e) , duration: getDurationInMilliseconds(start) }
      }
      console.log({ result })
      nats.publish(replyTo, JSON.stringify(result))
    } else {
      nats.publish(replyTo, JSON.stringify({ success: false, message: "Not valid message", duration: getDurationInMilliseconds(start) }));
    }
  })

  nats.subscribe('launch', { queue: 'job.workers' }, async (msg, replyTo) => {
    console.log(`requested the msg: (${msg})`)
    let valid_msg = check_message(msg);
    console.log("[msg valid]: " + valid_msg)
    // Start to do Work
    const start = process.hrtime();
    // Valid msg do Wokr
    if (valid_msg) {
      let result = {};
      try {
        // exec.create_and_launch => {success:boolean, message:String}
        result = await exec.create_and_launch(valid_msg.function_name, valid_msg.code)// compile an expression
        const durationInMilliseconds = getDurationInMilliseconds(start)
        result.duration = durationInMilliseconds;
        result.function_id = valid_msg.function_id;
        result.instance=INSTANCE;
        console.log({ result })
      } catch (e) {
        result = { success: false, message: "Error", function_id: valid_msg.function_id, instance:INSTANCE , duration: getDurationInMilliseconds(start) }
      }
     
      nats.publish(replyTo, JSON.stringify(result))
    } else {
      nats.publish(replyTo, JSON.stringify({ success: false, message: "Not valid message", duration: getDurationInMilliseconds(start) }));
    }
  })


  nats.subscribe('kill-worker', { queue: 'kill.workers' }, (msg, replyTo) => {
    console.log(`requested kill-worker(${msg})`)
    nats.publish(replyTo, "When I'm done I will die")
    nats.unsubscribe('kill-worker');
    nats.unsubscribe('launch');
    nats.unsubscribe('launch-maths');
    disconnect()
  })
  return true
}

function convert_to_array_integers(string){
  try{
  arrayNumbers = string.split(',').map(Number);
  if(check_params_maths(arrayNumbers))
    return arrayNumbers
  else return null
  }catch(e){
    console.log(e)
    return null
  }

}

function check_params_maths(array){
  if(Array.isArray(array))
    return array.every(function(element) {return typeof element === 'number';});
  else return false;
}

function set_mathscope(array,params_function){
let scope={};
if(array && Array.isArray(array))
  for (var i = 0; i < array.length && i<params_function.length ; i++)
    scope[params_function[i]]=array[i]
else return null;
return scope;
} 
function IsJsonString(str) {
  try {
    json = JSON.parse(str);
  } catch (e) {
    return false;
  }
  return json;
}
function check_message(msg) {
  json_msg = IsJsonString(msg)
  if (!json_msg) return false
  if (json_msg.hasOwnProperty("code") && json_msg.hasOwnProperty("function_name") && json_msg.hasOwnProperty("function_id"))
    return json_msg
  else return false
}

getDurationInMilliseconds = (start) => {
  const NS_PER_SEC = 1e9
  const NS_TO_MS = 1e6
  const diff = process.hrtime(start)
  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
}


console.log('[W] started worker')
console.log(`[W] Connecting to ${NATS_URL}`)

connectNATS().then(value => {
  init_subs()
}, reason => {
  console.error(`[W] ${reason}`);
});

