Anotaciones:
Exlicar el despliegue para el tema de cluster HA con DB
Lo mismo para nats, con lo que expone el la documentacion en su Dokcer Hub de la imagen

Mejoras:

Watchamn:
Instanciar workers
Aspectos de roles de usuarios etc..

DB:


Paso de mejora: Sincronizar todos los relojes de las instancias de db desplegadas:
https://www.cockroachlabs.com/docs/v20.2/deploy-cockroachdb-on-premises-insecure#requirements
Paso de mejora: 5. Set up load balancing
https://www.cockroachlabs.com/docs/v20.2/deploy-cockroachdb-on-premises-insecure#requirements


Nats:

Cluster HA exitosos:

[0] CH: natscluster INS: 3 IPS: 172.17.223.52  IP_HOST: 172.17.223.52 Nodo: 0
[17] 2021/01/26 19:36:58.068483 [INF] Starting nats-server version 2.1.8
[17] 2021/01/26 19:36:58.068660 [DBG] Go build version go1.14.8
[17] 2021/01/26 19:36:58.068665 [INF] Git commit [c0b574f]
[17] 2021/01/26 19:36:58.069712 [INF] Listening for client connections on 0.0.0.0:4222
[17] 2021/01/26 19:36:58.069743 [INF] Server id is NA2ALJ27QWVJNV5PUCX4SPYHODDGYHYZNXAPK2V7IUFLSI3VLMLR7AQ3
[17] 2021/01/26 19:36:58.069747 [INF] Server is ready
[17] 2021/01/26 19:36:58.069827 [DBG] Get non local IPs for "0.0.0.0"
[17] 2021/01/26 19:36:58.070263 [DBG]  ip=172.17.223.52
[17] 2021/01/26 19:36:58.070622 [INF] Listening for route connections on 172.17.223.52:4248
[17] 2021/01/26 19:36:58.071784 [DBG] Trying to connect to route on 172.17.223.52:4248
[17] 2021/01/26 19:36:58.072668 [INF] 172.17.223.52:52324 - rid:1 - Route connection created
[17] 2021/01/26 19:36:58.072785 [DBG] 172.17.223.52:4248 - rid:2 - Route connect msg sent
[17] 2021/01/26 19:36:58.164579 [INF] 172.17.223.52:4248 - rid:2 - Route connection created
[17] 2021/01/26 19:36:58.165465 [INF] 172.17.223.52:4248 - rid:2 - Router connection closed: Duplicate Route
[17] 2021/01/26 19:36:58.165558 [TRC] 172.17.223.52:52324 - rid:1 - <<- [CONNECT {"echo":true,"verbose":false,"pedantic":false,"tls_required":false,"name":"NA2ALJ27QWVJNV5PUCX4SPYHODDGYHYZNXAPK2V7IUFLSI3VLMLR7AQ3"}]
[17] 2021/01/26 19:36:58.165825 [DBG] Detected route to self, ignoring "nats://172.17.223.52:4248"
[17] 2021/01/26 19:36:58.165892 [INF] 172.17.223.52:52324 - rid:1 - Router connection closed: Duplicate Route
[17] 2021/01/26 19:37:00.689623 [DBG] 172.17.129.238:59586 - cid:3 - Client connection created
[17] 2021/01/26 19:37:00.693409 [TRC] 172.17.129.238:59586 - cid:3 - <<- [CONNECT {"lang":"node","version":"1.4.12","verbose":true,"pedantic":false,"protocol":1}]
[17] 2021/01/26 19:37:00.693596 [TRC] 172.17.129.238:59586 - cid:3 - ->> [OK]
[17] 2021/01/26 19:37:00.693815 [TRC] 172.17.129.238:59586 - cid:3 - <<- [PING]
[17] 2021/01/26 19:37:00.693826 [TRC] 172.17.129.238:59586 - cid:3 - ->> [PONG]
[17] 2021/01/26 19:37:02.711577 [DBG] 172.17.129.238:59586 - cid:3 - Client Ping Timer
[17] 2021/01/26 19:37:02.711767 [TRC] 172.17.129.238:59586 - cid:3 - ->> [PING]
[17] 2021/01/26 19:37:02.714148 [TRC] 172.17.129.238:59586 - cid:3 - <<- [PONG]
[17] 2021/01/26 19:37:04.141688 [INF] 172.17.86.50:34464 - rid:4 - Route connection created
[17] 2021/01/26 19:37:04.142144 [TRC] 172.17.86.50:34464 - rid:4 - <<- [CONNECT {"echo":true,"verbose":false,"pedantic":false,"tls_required":false,"name":"NA6I7JBK2K6T4F333CWFPC3PZR73XKSLG3ASFR5VF6SUXKPP5RM6GUO7"}]
[17] 2021/01/26 19:37:04.142293 [DBG] 172.17.86.50:34464 - rid:4 - Registering remote route "NA6I7JBK2K6T4F333CWFPC3PZR73XKSLG3ASFR5VF6SUXKPP5RM6GUO7"
[17] 2021/01/26 19:37:04.142515 [DBG] 172.17.86.50:34464 - rid:4 - Sent local subscriptions to route
[17] 2021/01/26 19:37:05.247480 [DBG] 172.17.86.50:34464 - rid:4 - Router Ping Timer
[17] 2021/01/26 19:37:05.247594 [TRC] 172.17.86.50:34464 - rid:4 - ->> [PING]
[17] 2021/01/26 19:37:05.248901 [TRC] 172.17.86.50:34464 - rid:4 - <<- [PONG]
[17] 2021/01/26 19:37:05.341727 [TRC] 172.17.86.50:34464 - rid:4 - <<- [PING]
[17] 2021/01/26 19:37:05.341756 [TRC] 172.17.86.50:34464 - rid:4 - ->> [PONG]
[17] 2021/01/26 19:37:10.255758 [INF] 172.17.154.92:49618 - rid:5 - Route connection created
[17] 2021/01/26 19:37:10.256252 [TRC] 172.17.154.92:49618 - rid:5 - <<- [CONNECT {"echo":true,"verbose":false,"pedantic":false,"tls_required":false,"name":"ND3WFROHJR27XBZMYJLOIWTGJ2VN6ZFBGT4MJSBA5JLQJYYJSJJKEQZA"}]
[17] 2021/01/26 19:37:10.256429 [DBG] 172.17.154.92:49618 - rid:5 - Registering remote route "ND3WFROHJR27XBZMYJLOIWTGJ2VN6ZFBGT4MJSBA5JLQJYYJSJJKEQZA"
[17] 2021/01/26 19:37:10.256486 [DBG] 172.17.154.92:49618 - rid:5 - Sent local subscriptions to route
[17] 2021/01/26 19:37:11.286373 [TRC] 172.17.154.92:49618 - rid:5 - <<- [PING]
[17] 2021/01/26 19:37:11.286401 [TRC] 172.17.154.92:49618 - rid:5 - ->> [PONG]
[17] 2021/01/26 19:37:11.333752 [DBG] 172.17.154.92:49618 - rid:5 - Router Ping Timer
[17] 2021/01/26 19:37:11.333848 [TRC] 172.17.154.92:49618 - rid:5 - ->> [PING]
[17] 2021/01/26 19:37:11.334641 [TRC] 172.17.154.92:49618 - rid:5 - <<- [PONG]
