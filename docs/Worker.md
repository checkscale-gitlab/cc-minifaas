# Worker:

 Desde la plataforma Kumori se crean una o más instancias de este componente, encargado de la ejecución de las funciones. Para la ejecución, cada nodo que se instancia se subscribe a la cola de trabajos de NATS, mediante la cual recibe información del trabajo a realizar.

#### Implementación

Se ha implementado bajo el lenguaje nodejs juntos a una serie de paquetes esenciales:

- nats
- jsonschema
- maths

Este componente esta subscrito a dos colas diferentes para cada tipo de función o lanzamiento creado,  es importante destacar que el contenido recibido previamente ha sido filtrado por el **Frontend**.

**Topics y colas**

El Worker se subscribe un topic para cada tipo de función, con una cala especifica para cada uno

Se ha especificado una cola, porque el servidor equilibrará la carga entre todos los miembros del grupo de cola. Además en una configuración de clúster, todos los miembros tienen las mismas posibilidades de recibir un mensaje en particular

------

**Topic**: `launch`
**Cola**: `job.mathematics.workers`

Recibe:
```json
 {
  function_id: '100',
  type: 'maths',
  function_name: 'resta',
  code: '100-10',
  params: null,
  creator: '631844972575227905',
  size: '100'
}
```

Envía:
```json
result: {
    message: 90,
    duration: 3.642554,
    function_id: '100',
    success: true,
    instance: 'kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572'
  }
```
------

**Topic**: `launch`
**Cola**: `job.workers`

Recibe:
```json
{ 
    function_id:"632094010400145409",
    type:"javascript",
    function_name:"fibonacci",
    code:"function fibonacci(num){var a = 1, b = 0, temp;while (num >= 0){temp = a;a = a + b;b = temp;num--;}return b;};console.log(fibonacci(24))",
    params:null,
    creator:"631844972575227905",
    size:226}
```
Envía:
```json
{
  result: {
    message: '75025\n',
    success: true,
    duration: 64.74361,
    function_id: '632094010400145409',
    instance: 'kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572'
  }
}
```
