# CockroachDB

La base de datos ha sido implementada con Cockroachdb implementado con clúster HA, para añadir tolerancia a fallos y replicación.

### Configuración modo clúster

Para poder realizar dicha configuración de clúster HA, primero ha sido creado un script para determinar que nodo actúa como principal y luego el script debe ser indicado *entrypoint* del componente. Gracias al script que se muestra a continuación se permite el uso de clústers:

```bash
#!/bin/sh
set -eu
if [ "${1-}" = "shell" ]; then
  shift
  exec /bin/sh "$@"
else
  echo "[INIT] RUN INIT SCRIPT"
  sleep $WAIT
  myip=`hostname -I | tr -d '[[:space:]]'`
  nodo=${HOSTNAME:${#HOSTNAME}-1:1}
  #GET IP MAIN SERVER DB VIA ENVIRONMENT VARIABLE -> INSTANCE (NUM of current instances)
  ip_maindb=$(nslookup $CHANNEL | awk '/Address/{print}' | sed 's/Address: //' | tail -n $INSTANCE | awk '{print}' ORS=',' | sed 's/.$//')
  #Information
  echo "[0] CH: $CHANNEL MAIN INS: $INSTANCE IPS: $ip_maindb  IP_HOST: $myip Nodo: $nodo"
  # --join all to maindb
  ./cockroach start --insecure --listen-addr=$myip:26257 --http-addr=$myip:8080 --join=$ip_maindb --background
  #INIT SERVICE ONLY MAIN DB = NODO 0
  if [ "$nodo" = "0" ]; then
      echo "[1] Main db, trying to init db"
      sleep 20
      ./cockroach init --insecure --host=$myip:26257
  else
      echo "[1] Node not main, trying to connect to main db"
  fi
#Check each 60s status node cockroachdb
  while sleep 60; do
  if pgrep "cockroach" >/dev/null 2>&1; then
        echo "[2] cockroach service OKEY:"
        ./cockroach node status --host=$myip:26257 --insecure
    else
      echo "[2] cockroach service not working FAILED"
      exit 1
  fi
  done
fi
```

Este script cada minuto realizar una comprobación del estado del clúster:

```shell
id  address sql_address     started_at             updated_at    is_available is_live
1   172.17.154.175:26257    172.17.154.175:26257   16:54:43.405  true         true
2   172.17.86.19:26257      172.17.86.19:26257     16:54:43.980  true         true
3   172.17.80.37:26257      172.17.80.37:26257     16:54:44.577  true         true
```

**Aspecto a tener en cuenta**

CockroachDB replica y distribuye los datos entre los nodos y utiliza un protocolo Gossip para permitir que cada nodo localice datos en el clúster.

Se puede mejorar el funcionamiento del servicio Cockroachdb tanto en rendimiento como a escalabilidad. Este aspecto es detallado  en el siguiente documento: [Futuras Mejoras](./Mejoras.md)

### Base de datos

#### Tablas

```
 schema_name | table_name | type  | owner | estimated_row_count
--------------+------------+-------+-------+----------------------
  public      | functions  | table | root  |                   4
  public      | registers  | table | root  |                   4
  public      | results    | table | root  |                   6
  public      | users      | table | root  |                   2
```

**Tabla functions**

| column_name   | data_type     | is_nullable | column_default | indices                             |
| ------------- | ------------- | ----------- | -------------- | ----------------------------------- |
| function_id   | INT8          | false       | unique_rowid() | {primary,functions_function_id_key} |
| type          | VARCHAR(100)  | true        | NULL           | {}                                  |
| function_name | VARCHAR(100)  | true        | NULL           | {}                                  |
| code          | VARCHAR(1000) | true        | NULL           | {}                                  |
| params        | VARCHAR(1000) | true        | NULL           | {}                                  |
| creator       | INT8          | true        | NULL           | {}                                  |
| size          | INT8          | true        | NULL           | {}                                  |

**Tabla results**

| column_name | data_type     | is_nullable | column_default | indices   |
| ----------- | ------------- | ----------- | -------------- | --------- |
| res_id      | INT8          | false       | unique_rowid() | {primary} |
| function_id | INT8          | true        | NULL           | {}        |
| result      | VARCHAR(1000) | true        | NULL           | {}        |
| date        | TIMESTAMP(3)  | true        | NULL           | {}        |

**Tabla users**

| column_name | data_type    | is_nullable | column_default  | indices                      |
| ----------- | ------------ | ----------- | --------------- | ---------------------------- |
| user_id     | INT8         | false       | unique_rowid()  | {primary,users_username_key} |
| username    | VARCHAR(100) | true        | NULL            | {users_username_key}         |
| user_rol    | VARCHAR(10)  | true        | 'user':::STRING | {}                           |

**Tabla registers**

| column_name   | data_type    | is_nullable | column_default | indices   |
| ------------- | ------------ | ----------- | -------------- | --------- |
| reg_id        | INT8         | false       | unique_rowid() | {primary} |
| user_id       | INT8         | true        | NULL           | {}        |
| function_id   | INT8         | true        | NULL           | {}        |
| use_time      | FLOAT8       | true        | NULL           | {}        |
| instance_type | VARCHAR(100) | true        | NULL           | {}        |
| date          | TIMESTAMP(3) | true        | NULL           | {}        |