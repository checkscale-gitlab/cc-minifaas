package nats

import k "kumori.systems/kumori/kmv"

#Manifest: k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.minifaas"
    name: "faas_nats"
    version: [0,0,1]
  }

  description: {

    srv: {
      //PROBAR
      
      server: natserver: {
        protocol: "tcp"
        port:     4222
      }
      duplex: natscluserver: {
        protocol: "tcp"
        port:4248
      }
       duplex: natscluster: {
        protocol: "tcp"
        port:     6222
      }

     
    }

    config: {
      resource: {}
      parameter: {}
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: nats: k.#Container & {
      name: "nats"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        //tag: "bitnami/nats:latest"
        tag: "mamarbao/faas-nats:2.2"
      }
      entrypoint: ["init-nats.sh"]
      mapping: {
        filesystem: []
        env: {
          NATSERVER_PORT_ENV: "\(srv.server.natserver.port)",
          CHANNEL: "natscluster",
          INSTANCES: "3",
          WAIT: "4"
        }
      }
    }
  }
}

